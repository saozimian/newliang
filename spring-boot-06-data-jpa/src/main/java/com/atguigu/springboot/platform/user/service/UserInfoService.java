package com.atguigu.springboot.platform.user.service;

import java.util.List;
import java.util.Map;

import com.atguigu.springboot.entity.UserInfo;

public interface UserInfoService {
	public UserInfo findByUsername(String username);

	public List<Map<String, Object>> getlist();

	public boolean registerData(String username, String password);

	public boolean addUser(UserInfo userInfo);  
}
