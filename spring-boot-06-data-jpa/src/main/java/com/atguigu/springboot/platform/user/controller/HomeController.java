package com.atguigu.springboot.platform.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.aspectj.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.atguigu.springboot.common.LoggerUtil;
import com.atguigu.springboot.entity.UserInfo;
import com.atguigu.springboot.platform.user.service.UserInfoService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@Controller
public class HomeController {
	/* private final static Logger logger = Logger.getLogger(HomeController.class);//给类初始化日志对象
	 * 
*/  
	/*private static  Logger logger = LoggerFactory.getLogger(FileUtil.class);*/
	private Logger logger = LogManager.getLogger(HomeController.class);
	   @Autowired
       UserInfoService userInfoService;
	   @RequestMapping({ "/", "index" })  
	    public String index() { 
		   //logger.info(logger, "", "5555555555555555");
		   logger.info("14524352435243543");
	        return "/test/index";  
	    }  
	  
	    @RequestMapping(value = "/login", method = RequestMethod.GET)  
	    public String login() {  
	    	logger.info("1452435243524354377777777777");
	        return "/test/login";  
	    }  
	
	    @SuppressWarnings("unlikely-arg-type")
		@RequestMapping(value = "/login", method = RequestMethod.POST)  
	    public String login(HttpServletRequest request,String username,String password,boolean rememberMe, Model model, HttpSession session) {  
	        System.out.println("HomeController.login");  
	        logger.info("made");
	  /*      String hashAlgorithmName = "MD5";//加密方式
	        Object crdentials = "123456";//密码原值
	        Object salt = "superadmin"+crdentials;//盐值
	        int hashIterations = 2;//加密1024次
	        String result = new SimpleHash(hashAlgorithmName,crdentials,salt,hashIterations).toHex();
	        System.out.println(result);
	        */
	        
	        //对密码进行加密
	        //password=new SimpleHash("md5", password, ByteSource.Util.bytes(username.toLowerCase() + "md5"),2).toHex();
	        //如果有点击  记住我
	        UsernamePasswordToken usernamePasswordToken =new UsernamePasswordToken(username,password,rememberMe);
	        if(request.getParameter("rememberMe")!=null){
	        	usernamePasswordToken.setRememberMe(true);
	        }
	        //UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username,password);
	        Subject subject = SecurityUtils.getSubject();
	        try {
	            //登录操作
	            subject.login(usernamePasswordToken);
	            UserInfo user=(UserInfo) subject.getPrincipal();
	            //更新用户登录时间，也可以在ShiroRealm里面做
	            session.setAttribute("userInfo", user);
	            model.addAttribute("userInfo",user);
	            return "test/index";
	        } catch(Exception exception) {
		        @SuppressWarnings("unused")
				String msg="";
				// 登录失败从request中获取shiro处理的异常信息  
		        // shiroLoginFailure:就是shiro异常类的全类名  
	            //登录失败从request中获取shiro处理的异常信息 shiroLoginFailure:就是shiro异常类的全类名
	            if (UnknownAccountException.class.getName().equals(exception)) {  
	                System.out.println("UnknownAccountException -->帐号不存在：");  
	                msg = "UnknownAccountException -->帐号不存在：";  
	            } else if (IncorrectCredentialsException.class.getName().equals(exception)) {  
	                System.out.println("IncorrectCredentialsException -- > 密码不正确：");  
	                msg = "IncorrectCredentialsException -- > 密码不正确：";  
	            } else if ("kaptchaValidateFailed".equals(exception)) {  
	                System.out.println("kaptchaValidateFailed -- > 验证码错误");  
	                msg = "kaptchaValidateFailed -- > 验证码错误";  
	            }else {  
	                msg = "else >> " + exception;  
	                System.out.println("else -- >" + exception);  
	            }  
	            model.addAttribute("msg","用户名或者密码错误！");
	            //返回登录页面
	            return "test/login";
	        }
	        
   }

	    @RequestMapping("/doRegister")
	    public String doRegister(@RequestParam("username") String username,
	                             @RequestParam("password") String password) {
	        boolean result = userInfoService.registerData(username,password);
	        if(result){
	            return "/test/login";
	        }
	        return "/test/index";
	    }
	    
	    
	    
}