package com.atguigu.springboot.config.LogInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


public class LogCostInterceptor implements HandlerInterceptor  {
	long start = System.currentTimeMillis();
	private static Logger logger = Logger.getLogger(LogCostInterceptor.class);
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
        start = System.currentTimeMillis();
        return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		 String requestURI = request.getRequestURI();
	     logger.info("请求路径 ："+requestURI);
		 StringBuffer requestURL = request.getRequestURL();
         logger.info("请求路径 ："+requestURL);
         String queryString = request.getQueryString();
         logger.info("请求参数 ："+queryString);
         StringBuffer append = requestURL.append("?").append(queryString);
         logger.info("请求完整路径： "+append);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}

}
