package com.atguigu.springboot.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class MyExceptionResolver implements HandlerExceptionResolver {
	//private static final Logger logger = Logger.getLogger(MyExceptionResolver.class);
	private static  Logger logger = LoggerFactory.getLogger(FileUtil.class);
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		        Map<String, Object> model = new ConcurrentHashMap<String, Object>();
		        model.put("msg", ex.getMessage());
		        // 可以细化异常信息，给出相应的提示
		        logger.info("==========发生了异常：");
		        logger.info("==========异常类型："+ex.getClass().getSimpleName());
		        logger.info("==========异常描述："+ex.getMessage());
		        logger.info("==========异常原因："+ex.getCause());
		        int status = response.getStatus();
		        System.out.println(status);
		        return new ModelAndView("403",model);
	}
}
