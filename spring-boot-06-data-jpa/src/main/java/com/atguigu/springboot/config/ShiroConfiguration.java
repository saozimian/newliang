package com.atguigu.springboot.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;  
  
@Configuration  
public class ShiroConfiguration {  
  
    @Bean  
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {  
        System.out.println("ShiroConfiguration.shiroFilter()");  
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();  
  
        // 必须设置SecuritManager  
        shiroFilterFactoryBean.setSecurityManager(securityManager);  
        // 拦截器  
        Map<String, String> filtermap = new LinkedHashMap<String, String>();  
        // 配置退出过滤器,其中的具体代码Shiro已经替我们实现了  
        filtermap.put("/logout", "logout");  
        // <!– 过滤链定义，从上向下顺序执行，一般将 /**放在最为下边 –>:这是一个坑呢，一不小心代码就不好使了;  
        // <!– authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问–>  
        filtermap.put("/getlist", "anon");  
        filtermap.put("/doRegister", "anon");  
        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面  
        shiroFilterFactoryBean.setLoginUrl("/login");  
        // 登录成功后要跳转的链接  
        shiroFilterFactoryBean.setSuccessUrl("/index");  
        // 未授权界面;  
        shiroFilterFactoryBean.setUnauthorizedUrl("/403");  
        // filtermap.put("/**", "authc");  
        filtermap.put("/*", "user");  
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filtermap);  
        return shiroFilterFactoryBean;  
  
    }  
  //自定义的Realm
    @Bean  
    public SecurityManager securityManager() {  
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();  
        ///添加自定义的Realm
        //设置realm.  
        securityManager.setRealm(myShiroRealm()); 
        //配置记住我 参考博客：
        securityManager.setRememberMeManager(rememberMeManager());
        //配置 redis缓存管理器 参考博客：
        //securityManager.setCacheManager(getEhCacheManager());
        //配置自定义session管理，使用redis 参考博客：
        //securityManager.setSessionManager(sessionManager());
        return securityManager;  
    }  
  
    /**  
     * 身份认证realm;  
     *   
     */  
    @Bean  
    public MyShiroRealm myShiroRealm(){  
       MyShiroRealm myShiroRealm = new MyShiroRealm(); 
       myShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());
       return myShiroRealm;  
    }       
  
    /**  
     * 凭证匹配器  
     * （由于我们的密码校验交给Shiro的SimpleAuthenticationInfo进行处理了  
     *  所以我们需要修改下doGetAuthenticationInfo中的代码;  
     * ）  
     * @return  
     */  
    @Bean  
    public HashedCredentialsMatcher hashedCredentialsMatcher(){  
       HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();  
       hashedCredentialsMatcher.setHashAlgorithmName("md5");//散列算法:这里使用MD5算法;  
       hashedCredentialsMatcher.setHashIterations(2);//散列的次数，比如散列两次，相当于 md5(md5(""));  
       return hashedCredentialsMatcher;  
    } 
    
  /* *//** 注册身份验证
    * @param hashedCredentialsMatcher 凭证匹配器
    * @return
    *//*
   @Bean
   public MyShiroRealm oAuth2Realm(HashedCredentialsMatcher hashedCredentialsMatcher) {
	   MyShiroRealm oAuth2Realm = new MyShiroRealm();
       oAuth2Realm.setCredentialsMatcher(hashedCredentialsMatcher);
       return oAuth2Realm;
   }*/
    /**
     * 
     * @描述：开启Shiro的注解(如@RequiresRoles,@RequiresPermissions),
     * 需借助SpringAOP扫描使用Shiro注解的类,并在必要时进行安全逻辑验证
     * 配置以下两个bean(DefaultAdvisorAutoProxyCreator和AuthorizationAttributeSourceAdvisor)即可实现此功能
     * </br>Enable Shiro Annotations for Spring-configured beans. Only run after the lifecycleBeanProcessor
     * (保证实现了Shiro内部lifecycle函数的bean执行) has run
     * </br>不使用注解的话，可以注释掉这两个配置
     * @创建时间：2018年5月21日 下午6:07:56
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }
    //异常统一处理
	@Bean
	public HandlerExceptionResolver solver(){
		HandlerExceptionResolver handlerExceptionResolver=new MyExceptionResolver();
		return handlerExceptionResolver;
	}

    /**  
     *  开启shiro aop注解支持.  
     *  使用代理方式;所以需要开启代码支持;  
     * @param securityManager  
     * @return  
     */  
    @Bean  
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){  
       AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();  
       authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);  
       return authorizationAttributeSourceAdvisor;  
    }  
    /**
     * cookie对象;会话Cookie模板 ,默认为: JSESSIONID 问题: 与SERVLET容器名冲突,重新定义为sid或rememberMe，自定义
     * @return
     */
    @Bean
    public SimpleCookie rememberMeCookie(){
        //这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        //setcookie的httponly属性如果设为true的话，会增加对xss防护的安全系数。它有以下特点：

        //setcookie()的第七个参数
        //设为true后，只能通过http访问，javascript无法访问
        //防止xss读取cookie
        simpleCookie.setHttpOnly(true);
        simpleCookie.setPath("/");
        //<!-- 记住我cookie生效时间30天 ,单位秒;-->
        //simpleCookie.setMaxAge(2592000);
        simpleCookie.setMaxAge(2592);
        return simpleCookie;
    }
    
    /**
     * cookie管理对象;记住我功能,rememberMe管理器
     * @return
     */
    @Bean
    public CookieRememberMeManager rememberMeManager(){
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookie());
        //rememberMe cookie加密的密钥 建议每个项目都不一样 默认AES算法 密钥长度(128 256 512 位)
        cookieRememberMeManager.setCipherKey(Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        return cookieRememberMeManager;
    }
    /**
     * FormAuthenticationFilter 过滤器 过滤记住我
     * @return
     */
    @Bean
    public FormAuthenticationFilter formAuthenticationFilter(){
        FormAuthenticationFilter formAuthenticationFilter = new FormAuthenticationFilter();
        //对应前端的checkbox的name = rememberMe
        formAuthenticationFilter.setRememberMeParam("rememberMe");
        return formAuthenticationFilter;
    }
    
}  