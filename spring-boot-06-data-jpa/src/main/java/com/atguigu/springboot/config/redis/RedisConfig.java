package com.atguigu.springboot.config.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class RedisConfig extends CachingConfigurerSupport{

	/*
	    *通过RedisConnectionFactory引入redis在配置文件中的连接配置
	    * */
	    @Autowired
	    private RedisConnectionFactory factory;
	    /*
	    * 1.此处为什么要序列化
	    * 原因：普通的连接使用没有办法把Java对象直接存入Redis，
	    * 而需要我们自己提供方案-对象序列化，然后存入redis，
	    * 取回序列化内容后，转换为java对象
	    * */
	    @Bean
	    public RedisTemplate<String,Object> redisTemplate(){
	        RedisTemplate<String,Object> template = new RedisTemplate<>();
	        initDomainRedisTemplate(template,factory);
	        return template;
	    }

	    @SuppressWarnings("unchecked")
		public void initDomainRedisTemplate(@SuppressWarnings("rawtypes") RedisTemplate template,RedisConnectionFactory factory){
	        template.setConnectionFactory(factory);
	        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的 value 值
	         Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);
	        ObjectMapper mapper = new ObjectMapper(); //ObjectMapper是jackson的主要类，作用序列化
	         mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY); //设置任何字段可见
	         mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL); //反序列化
	         serializer.setObjectMapper(mapper);

	        template.setValueSerializer(serializer); //设置序列化反序列化redis的value值
	         template.setKeySerializer(new StringRedisSerializer()); //设置序列化反序列化key值的方式为String
	        template.afterPropertiesSet(); //初始化操作，在设置完成后进行
	    }

	    //实例化HashOperations对象，可以使用Hash类型操作
	    @Bean
	    public HashOperations<String,String,Object> hashOperations(RedisTemplate<String,Object> redisTemplate){
	        return redisTemplate.opsForHash(); //opsForHash()该方法用于操作Hash
	    }

	    //实例化ListOperations对象，可以使用List类型操作
	    @Bean
	    public ListOperations<String,Object> listOperations(RedisTemplate<String,Object> redisTemplate){
	        return redisTemplate.opsForList(); //opsForList()该方法用于操作List
	    }
	
	
}
