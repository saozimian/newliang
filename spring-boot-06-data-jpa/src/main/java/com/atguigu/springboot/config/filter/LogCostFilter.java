/*package com.atguigu.springboot.config.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.boot.web.servlet.ServletComponentScan;

*//**
 * 这里直接用@WebFilter就可以进行配置，同样，可以设置url匹配模式，过滤器名称等。
 * 这里需要注意一点的是@WebFilter这个注解是Servlet3.0的规范，并不是Spring boot提供的。
 * 除了这个注解以外，我们还需在配置类中加另外一个注解：@ServletComponentScan，指定扫描的包。
 * @author Administrator
 *
 *//*

public class LogCostFilter implements Filter  {
	private static Logger logger = Logger.getLogger(LogCostFilter.class);
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		     long start = System.currentTimeMillis();
		     chain.doFilter(request,response);
		     String contextPath = request.getServletContext().getContextPath();
		     System.out.println(contextPath+"项目路径");
	         System.out.println("Execute cost="+(System.currentTimeMillis()-start));
	         HttpServletRequest req= (HttpServletRequest)request;
	         String requestURI = req.getRequestURI();
	         logger.info(requestURI);
	         String queryString = req.getQueryString();
	         logger.info(queryString);
	}
	@Override
	public void destroy() {
	}


}
*/