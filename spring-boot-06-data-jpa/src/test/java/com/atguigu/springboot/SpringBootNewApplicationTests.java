package com.atguigu.springboot;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@SpringBootApplication
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootNewApplicationTests {


}
